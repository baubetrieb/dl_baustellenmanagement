# BaustellenManagement

Herunterladen von Dateien für die Ausführung des Programmes BaustellenManagement.

Das Programm Baustellenmanagement soll die Führungskräfte einer Baustelle bei der Planung und der Steuerung der Baustelle unterstützen. Dabei soll der virtuelle Raum im Sinne eines digitalen Zwillings wesentliche Merkmale der Baustelle wiedergeben und der Bauleiterin, dem Bauleiter die Möglichkeit geben, Situationen vor der Bauausführung gedanklich zu überprüfen.

Die aktuelle Programmversion wird im Modulen des Baubetriebs in den Studiengängen "europäisches Baumanagement" und "Bauingenieurwesen" an der htw saar eingesetzt.

Das Programm soll sich langfristig aus zwei Bereichen zusammensetzen. Der erste Bereich ist die Planung der Baustelle, an diesem wird aktuell programmiert und erste Elemente einer Baustellenplanung sind funktionsfähig vorhanden. Der zweite Bereich umfasst die Steuerung einer Baustelle, dieser Bereich ist programmtechnisch noch nicht erfasst.

**Planung der Baustelleneinrichtung**
Für die Planung der Baustelleneinrichtung ist es erforderlich die Objekte des Bauwerkes in das Programm einzulesen, dies geschieht über das IFC-Format (zugegebenerweise ist die Vorgehensweise noch nicht sehr benutzerfreundlich). Im weiteren sind im Programm eine Auswahl von Baumaschinen und eine Auswahl von sonstigen Einrichtungselementen über ein Auswahlmenü abrufbar (Kennzahlen zu den Betriebsmitteln sollten in einer Datenbank abgelegt werden, die Funktion muss überarbeitet werden). Der Bauablauf wird über im Programm definierte Bauabschnitte abgebildet (es können neue Bauabschnitte eingerichtet werden, aber aktuell können keine Bauabschnitte gelöscht werden).

Alle Objekte können im virtuellen Raum frei bewegt werden und auch wieder gelöscht werden.

Als Umgebung für die Einrichtung der Baustelle, ist aktuell nur eine fest eingerichtete Umgebung vorhanden.

Die geplante Baustelleneinrichtung kann gespeichert und jederzeit wieder aufgerufen werden.

**weitere Programmentwicklung**
Die Programmentwicklung läuft über Projekte im Studiengang "Praktische Bauinformatik" an der htw saar. Aber wir freuen uns über Jeden oder Jede, die uns bei der weiteren Entwicklung unterstützen will.

Die nächsten Aufgaben für die Weiterentwicklung der Programmierung wären:
- Übertragung des virtuellen Raums in eine Ouculus Rift.
- Überarbeitung und Weiterentwicklung des Zugriffes auf die Datenbank.
- Überarbeitung und Verbesserung von einzelnen Funktionen im Programm, wie zum Beispiel dem Löschen von Bauabschnitten.
